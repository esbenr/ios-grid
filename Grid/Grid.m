#import "Grid.h"

@implementation Grid
@synthesize dataSource = _dataSource;

- (void)layoutSubviews
{
    [super layoutSubviews];

    for (UIView *v in self.subviews)
        [v removeFromSuperview];

    CGSize s = [self.dataSource sizeForViews];
    float columnWidth = self.frame.size.width / (s.width);
    int columns = (int)columnWidth;
    int rows = (int)ceil((float)[self.dataSource numberOfViews]/(float)columns);

    float rest = (columnWidth - columns)*s.width;
    float margin = rest/(columns+1);


    int contentHeight = (int)((rows*(s.height+margin))+margin);
    self.contentSize = CGSizeMake(columnWidth*s.width, contentHeight);

    for (NSUInteger i = 0; i < [self.dataSource numberOfViews] ; i++)
    {
        UIView *view = [self.dataSource viewForIndex:i];
        int x = i%columns;
        int y = i/columns;

        CGRect rect = CGRectMake(((x*s.width)+((x+1)*margin)), ((y*s.height)+((y+1)*margin)), s.width, s.height);
        /*
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{ */
                             [view setFrame:rect];
        /*                 }
                         completion:nil]; */

        [self insertSubview:view atIndex:0];
    }
}

@end