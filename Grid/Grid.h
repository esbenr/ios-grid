#import <Foundation/Foundation.h>

@protocol GridDataSource

@required
-(UIView*)viewForIndex:(NSUInteger)index;
-(NSUInteger)numberOfViews;
-(CGSize)sizeForViews;

@end

@interface Grid : UIScrollView

@property(nonatomic, assign) IBOutlet id<GridDataSource> dataSource;

@end