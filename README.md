# iOS Grid

This is a simple light weight grid / collection view for iOS apps.
The grid supports all iOS versions.

## Features

1. Datasource-like usage (like UITableView etc.) 
2. Show any view derived from UIView like a collection / gallery / grid
3. Supports any aspect ratio so your views can be quadrant, rectangles or what ever.
4. Supports rotation

##Screenshots

![iPhone portrait screenshot](http://dl.dropbox.com/u/21955281/Grid/iPhone-Portrait.png)

![iPhone landscape screenshot](http://dl.dropbox.com/u/21955281/Grid/iPhone-Landscape.png)

![iPad portrait screenshot](http://dl.dropbox.com/u/21955281/Grid/iPad-Portrait.png)

![iPad landscape screenshot](http://dl.dropbox.com/u/21955281/Grid/iPad-Landscape.png)
 
# How to use

1. In your UIViewController. Add a Grid (In IB add a UIScrollView and set it to type Grid)
2. Assign the datasource delegate to be your UIViewController.
3. Implement the three DataSource delegate methods
4. Run your app.

# Requirements 

Tested nd developed on iOS 5.1, but should be compatible with any iOS.  

# License

To be determined.
