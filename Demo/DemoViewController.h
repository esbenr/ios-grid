//
//  DemoViewController.h
//  Grid
//
//  Created by Rasmussen, Esben on 14/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Grid.h"

@class Grid;

@interface DemoViewController : UIViewController <GridDataSource>

@property(nonatomic, retain) IBOutlet Grid *grid;

@end
