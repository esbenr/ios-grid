//
//  DemoViewController.m
//  Grid
//
//  Created by Rasmussen, Esben on 14/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DemoViewController.h"

@interface DemoViewController ()

@end

@implementation DemoViewController
@synthesize grid = _grid;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark GridDataSource

- (UIView *)viewForIndex:(NSUInteger)index
{
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%02d.jpg", index+1]];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    return imageView;
}

- (NSUInteger)numberOfViews
{
    return 19;
}

- (CGSize)sizeForViews
{
    CGSize result = CGSizeMake(150, 80);
    return result;
}

@end
